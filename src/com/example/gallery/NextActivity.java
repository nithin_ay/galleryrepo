package com.example.gallery;



import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class NextActivity extends Activity {
	ImageView mImageView;
	TextView mTextView;
	String mText;
	int mImg;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_next);
		mText = getIntent().getExtras().getString("text");
		mImg = getIntent().getExtras().getInt("image");
		mImageView = (ImageView) findViewById(R.id.img_next);
		mTextView = (TextView) findViewById(R.id.text_next);
		mImageView.setImageResource(mImg);
		mTextView.setText(mText);
		
	}
}
