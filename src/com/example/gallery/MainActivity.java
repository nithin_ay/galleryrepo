package com.example.gallery;





import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity
{
	private Context context;
	GridView mGridView;
	
	String[] names = {"Kate","Bean","Brad","Jolie","Bale","yo","Aish","Hugh","Black"," De Caprio","Cowboy"," Batman","Joker","Tom","    Spiderman"};
	int[] images= {R.drawable.pic_1,R.drawable.pic_2,R.drawable.pic_3,R.drawable.pic_4,R.drawable.pic_5,R.drawable.pic_6,R.drawable.pic_7,R.drawable.pic_8,R.drawable.pic_9,R.drawable.pic_10,R.drawable.pic_11,R.drawable.pic_12,R.drawable.pic_13,R.drawable.pic_14,R.drawable.pic_15};
	

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mGridView = (GridView) findViewById(R.id.grid1);
//		for(int i=0;i<15;i++) {
   
			
			mGridView.setAdapter(new GalaryClass(images,names));
			System.out.println("i m in for loop");
		
		
//		}
			
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	class GalaryClass extends BaseAdapter  {
		ImageView mImageView;
		TextView mTextView;
		LayoutInflater inflater=getLayoutInflater();
		MyViewHolder mViewHolder;
		String[] nameId;
		Context contextIs;
		
		int[] imageId;
		public GalaryClass(int[] img,String[] name){
			nameId = name ;
			imageId = img ;
			contextIs = context;
			System.out.println("i m in constructor");
			System.out.println("i m in constructor");
			
//			inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			System.out.println("i m in getCount");
			
			return names.length;
			
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			System.out.println("i m in getView");
			// TODO Auto-generated method stub
			View element=convertView;
			if(element == null) {
    	   
 element = inflater.inflate(R.layout.element,parent,false);
// 	  element = ListView listView = (ImageView) menuItem.getActionView();
   
    Log.i("IsNull : ", "Null");
 } else {
    mViewHolder = (MyViewHolder) element.getTag();
    Log.i("IsNull : ", "Not Null");
 }
			 mViewHolder  = new MyViewHolder();
			    mViewHolder.name = (TextView) element.findViewById(R.id.text);
			    mViewHolder.name.setText(nameId[position]);
			    mViewHolder.img  = (ImageView) element.findViewById(R.id.img);
			    mViewHolder.img.setImageResource(imageId[position]);
			    element.setTag(mViewHolder);
			 element.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(MainActivity.this, NextActivity.class);
					
					intent.putExtra("image", imageId[position]);
					intent.putExtra("text",nameId[position] );
					startActivity(intent);
					
				}
			}) ; 

			return element;
		}
		
		
		
	}
	private class MyViewHolder {
   TextView name;
   ImageView img;
}
}
